import { Injectable } from '@angular/core';
import { ApiCallService } from './api-call.service';

@Injectable()
export class MovieService {
  constructor(
    private apiCallService: ApiCallService
  ) { }

  getMovie(id: string): any {
    return this.apiCallService.get('&i=' + id);
  }

  findMovies(queryParam: string): any {
    return this.apiCallService.get('&s=' + queryParam);
  }
}
