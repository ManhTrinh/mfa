import { NgModule } from '@angular/core';

// API
import {
  MovieResolver,
  ApiCallService,
  MovieService
} from '.';

@NgModule({
  declarations: [],
  imports: [],
  providers: [
    ApiCallService,
    MovieService,
    MovieResolver
  ],
})
export class ApiModule {}
