import { MovieService } from 'src/app/api/movie.service';
import { Injectable } from '@angular/core';
import { Movie } from './../models/movie.model';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class MovieResolver implements Resolve<Movie[]> {
  constructor(
    private movieservice: MovieService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    if (route.params['id']) {
      return this.movieservice.getMovie(route.params['id']);
    } else if (route.queryParamMap.has('s')) {
      return this.movieservice.findMovies(route.queryParamMap.get('s'));
    } else {
      this.router.navigate(['']);
    }
  }
}
