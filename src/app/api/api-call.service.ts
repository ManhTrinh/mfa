import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiCallService {

  constructor(
    private http: HttpClient
  ) { }

  getBasePath(): string {
    return environment.omdbapi.omdbapi_url + environment.omdbapi.omdbapi_key;
  }

  get(url: string): any {
    return this.http.get(this.getBasePath() + url);
  }
}
