export class Movie {
  Title: string;
  Year: string;
  Type: string;
  Plot?: string;
  Actors?: string;
  Genre?: string;
  imdbRating?: string;
  Runtime?: string;
  BoxOffice?: string;
  Awards?: string;
  Poster?: string;
}
