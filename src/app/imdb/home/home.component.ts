import { Movie } from 'src/app/models';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  topList: Movie[];
  sliders: any;
  constructor() { }

  ngOnInit() {
    this.topList = [
      { Title: 'Title 1', Year: '2001', Type: 'movie' },
      { Title: 'Title 2', Year: '2002', Type: 'movie' },
      { Title: 'Title 3', Year: '2003', Type: 'movie' },
      { Title: 'Title 4', Year: '2004', Type: 'movie' },
      { Title: 'Title 5', Year: '2005', Type: 'movie' },
      { Title: 'Title 6', Year: '2006', Type: 'movie' },
      { Title: 'Title 7', Year: '2007', Type: 'movie' },
      { Title: 'Title 8', Year: '2008', Type: 'movie' },
      { Title: 'Title 9', Year: '2009', Type: 'movie' },
      { Title: 'Title 10', Year: '2010', Type: 'movie' }
    ];

    this.sliders = [
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-1ggmcfs_3d03b7f7.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-45zqxw_af2aa0b6.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-4znydh_08b950e6.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-3jd9ks_46639fdc.jpeg?region=0%2C0%2C1280%2C420' }
    ];
  }

}
