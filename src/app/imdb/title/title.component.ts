import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from './../../models/movie.model';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  movie: Movie;
  sliders: any;
  constructor(route: ActivatedRoute ) {
    route.data.subscribe(
      data => this.movie = data['movie']
    );
  }

  ngOnInit() {
    this.sliders = [
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-1ggmcfs_3d03b7f7.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-45zqxw_af2aa0b6.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-4znydh_08b950e6.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-3jd9ks_46639fdc.jpeg?region=0%2C0%2C1280%2C420' }
    ];
  }
}
