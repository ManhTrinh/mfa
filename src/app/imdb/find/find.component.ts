import { Movie } from './../../models/movie.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})
export class FindComponent implements OnInit {
  searchTitle: string;
  movies: Movie[];
  sliders: any;

  constructor(public route: ActivatedRoute) {
    route.data.subscribe(
      data => {
        this.movies = data['movie'].Search;
        this.searchTitle = this.route.snapshot.queryParamMap.get('s');
      }
    );
  }

  ngOnInit() {
    this.sliders = [
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-1ggmcfs_3d03b7f7.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-45zqxw_af2aa0b6.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-4znydh_08b950e6.jpeg?region=0%2C0%2C1280%2C420' },
      { img: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-12561-3jd9ks_46639fdc.jpeg?region=0%2C0%2C1280%2C420' }
    ];
  }
}
