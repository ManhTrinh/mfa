import { NgModule } from '@angular/core';

import { ImdbRoutingModule } from './imdb-routing.module';
import { SharedModule } from './shared/shared.module';

// IMDB Components
import {
  ImdbComponent,
  HomeComponent,
  FindComponent,
  TitleComponent,
} from '.';

@NgModule({
  declarations: [
    ImdbComponent,
    HomeComponent,
    FindComponent,
    TitleComponent
  ],
  imports: [
    ImdbRoutingModule,
    SharedModule
  ],
  exports: [

  ],
  providers: [

  ],
})
export class ImdbModule {}
