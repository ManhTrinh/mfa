import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { MovieResolver } from './../api/movie.resolver';

import {
  TitleComponent,
  FindComponent,
  HomeComponent,
  ImdbComponent
} from '.';

const routes: Routes = [
  { path: '', component: ImdbComponent,
    children: [
      { path: '', component: HomeComponent },
      {
        path: 'find',
        component: FindComponent,
        resolve: {
          movie: MovieResolver
        },
        runGuardsAndResolvers: 'paramsOrQueryParamsChange',
      },
      {
        path: 'title/:id',
        component: TitleComponent,
        resolve: {
          movie: MovieResolver
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImdbRoutingModule {}
