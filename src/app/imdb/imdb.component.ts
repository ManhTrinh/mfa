import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-imdb',
  templateUrl: './imdb.component.html',
  styleUrls: ['./imdb.component.scss']
})
export class ImdbComponent implements OnInit {
  searchTerm: string;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParamMap.get('s')) {
      this.searchTerm = this.activeRoute.snapshot.queryParamMap.get('s');
    }
  }

  onSubmit() {
    this.router.navigate(['/find'], { queryParams: { s: this.searchTerm } });
  }
}
