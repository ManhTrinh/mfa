import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { ApiModule } from './../../api/api.module';

// At the moment we are importing all Material Components but we should only import what we need;
import { MaterialModule } from './material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

// Shared Components
import {
  MovieItemComponent,
  MovieSliderComponent,
  TopListComponent
} from '.';

@NgModule({
  declarations: [
    MovieItemComponent,
    MovieSliderComponent,
    TopListComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ApiModule,
    FormsModule,
    FlexLayoutModule,
    MaterialModule,
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    ApiModule,
    FormsModule,
    FlexLayoutModule,
    MaterialModule,
    MovieItemComponent,
    MovieSliderComponent,
    TopListComponent
  ],
  providers: [

  ],
})
export class SharedModule {}
