import { Component, OnInit, Input } from '@angular/core';
import { Movie } from 'src/app/models';

@Component({
  selector: 'app-top-list',
  templateUrl: './top-list.component.html',
  styleUrls: ['./top-list.component.scss']
})
export class TopListComponent implements OnInit {
  @Input() topList: Movie[];
  dataSource: any;
  displayedColumns: string[] = ['Title', 'Year', 'Type'];
  constructor() { }

  ngOnInit() {
    this.dataSource = this.topList;
  }

}
