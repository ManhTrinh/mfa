import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-movie-slider',
  templateUrl: './movie-slider.component.html',
  styleUrls: ['./movie-slider.component.scss']
})
export class MovieSliderComponent implements OnInit, OnDestroy {
  @Input() intervalToSlide = 5000;
  @Input() sliders;
  selectedIndex = 0;
  interval: any;
  constructor() { }

  ngOnInit() {
    this.initSlider();
  }

  initSlider(): void {
    this.interval = setInterval(() => {
      if (this.selectedIndex < this.sliders.length - 1) {
        this.selectedIndex++;
      } else {
        this.selectedIndex = 0;
      }
    }, this.intervalToSlide);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  prev(): void {
    if (this.selectedIndex > 0) {
      this.selectedIndex--;
    } else {
      this.selectedIndex = this.sliders.length - 1;
    }
  }

  next(): void {
    if (this.selectedIndex < this.sliders.length - 1) {
      this.selectedIndex++;
    } else {
      this.selectedIndex = 0;
    }
  }
}
